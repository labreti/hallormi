package hallo;

import java.rmi.Naming;

public class Client {
	public static void main(String[] args) throws Exception {
		HalloIF name = (HalloIF) Naming.lookup("rmi://192.168.5.1:1111/MyServer");
		System.out.println(name.sayHallo());
	}
}
