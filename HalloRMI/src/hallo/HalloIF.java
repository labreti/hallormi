package hallo;

import java.rmi.Remote;
import java.rmi.RemoteException;
 
public interface HalloIF extends Remote {
    public String sayHallo() throws RemoteException;
}
