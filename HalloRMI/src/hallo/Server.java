package hallo;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;

public class Server extends UnicastRemoteObject implements HalloIF {
	private static final long serialVersionUID = 1L;
	public Server() throws RemoteException {}
    public String sayHallo() throws RemoteException {
        try {
            return ("Hello " + Server.getClientHost() + " !");
        } catch (ServerNotActiveException ex) {
            return (ex.getMessage());
        }
    }
    public static void main(String[] args) throws RemoteException {
        System.setProperty("java.rmi.server.hostname", "192.168.5.1");
        Server objServer = new Server();
        Registry reg = LocateRegistry.createRegistry(1111);
        reg.rebind("MyServer", objServer);
        System.out.println("Server Ready");
    }
}
